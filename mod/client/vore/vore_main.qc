// --------------------------------
// Local functions:
// --------------------------------

bool Vore_Models_Internal_Needed()
{
	if(!autocvar_cl_vore_model_internal)
		return false;
	else if(!IS_SWALLOWED() && !IS_PREY())
		return false;
	else if(autocvar_chase_active || spectatee_status == -1)
		return false;
	return true;
}

bool Vore_Models_External_Needed()
{
	if(!autocvar_cl_vore_model_external)
		return false;
	else if(!IS_SWALLOWING() && !IS_PRED())
		return false;
	else if(autocvar_chase_active || spectatee_status == -1)
		return false;
	return true;
}

void Vore_Models_Internal_Draw(entity this)
{
	// remove the internal model when it's not needed
	if(!Vore_Models_Internal_Needed())
	{
		delete(this);
		internal = NULL;
		return;
	}

	//entity prey = ENT_SELF();
	entity pred = ENT_PRED();
	entity prey_mdl = MDL_SELF();
	entity pred_mdl = MDL_PRED();

	// get the name of the player model
	// this represents the mouth model while being swallowed and the stomach model once prey
	string internalmodel = playermodel_pure(pred.model);
	if(IS_PREY() && !button_zoom)
		internalmodel = strcat(internalmodel, "_stomach.iqm");
	else
		internalmodel = strcat(internalmodel, "_mouth.iqm");

	// hide and skip further updates if the model file doesn't exist
	if(!fexists(internalmodel))
	{
		print(strcat("Warning: Gullet model ", internalmodel, " not found. Your predator is using a broken player model!"));
		_setmodel(this, "");
		return;
	}

	// update the position of the internal model to match our prey position or progress
	if(IS_PREY() && !button_zoom)
	{
		// stomach model
		// don't use the pred's origin directly as it causes shaking, extract our view offset instead... also account for the vore offset
		this.origin = view_origin - STAT(PL_VIEW_OFS) - STAT(VORE_OFFSET);
		this.angles = vec3(0, pred.v_angle.y, 0);
	}
	else
	{
		// mouth model representing prey progress
		// the model must come from behind the view, use the X angle of the prey... additionally represent the pred's pitch, use the Y angle of the pred
		float scalediff = (prey_mdl.scale && pred_mdl.scale) ? (pred_mdl.scale / prey_mdl.scale) : 1; // offset swallow progress visual indication by scale difference
		float state = IS_PREY() ? 1 : min(1, STAT(VORE_PROGRESS_PREY) * scalediff);
		float dist = (-0.5 + state) * autocvar_cl_vore_model_internal_range; // progress ranges between -0.5 and 0.5
		vector viewangle_origin = vec3(-pred.v_angle.x, view_angles.y, 0);
		vector viewangle_angles = vec3(pred.v_angle.x, view_angles.y, 0);
		makevectors(viewangle_origin);
		this.origin = view_origin + (v_forward * dist);
		this.angles = viewangle_angles;
	}

	// update other properties
	if(internal.model != internalmodel)
	{
		_setmodel(this, internalmodel); // player model may change while the predator is registered
		this.frame = 0; // reset the animation if any
	}
	if(this.skin != pred.skin)
		this.skin = pred.skin; // player skin may change while the predator is registered
	if(this.colormap != pred.colormap)
		this.colormap = pred.colormap; // pants and shirt color
	if(this.glowmod != pred.glowmod)
		this.glowmod = pred.glowmod; // glow color
	this.alpha = autocvar_cl_vore_model_internal;
}

void Vore_Models_External_Draw(entity this)
{
	// remove the external model when it's not needed
	if(!Vore_Models_External_Needed())
	{
		delete(this);
		external = NULL;
		return;
	}

	//entity prey = ENT_PREY();
	entity pred = ENT_SELF();
	entity prey_mdl = MDL_PREY();
	entity pred_mdl = MDL_SELF();

	// get the name of the player model
	string externalmodel = strcat(playermodel_pure(pred.model), "_body.iqm");

	// hide and skip further updates if the model file doesn't exist
	if(!fexists(externalmodel))
	{
		print(strcat("Warning: Body model ", externalmodel, " not found. You are using a broken player model!"));
		_setmodel(this, "");
		return;
	}

	// update the position of the external model to match our predator progress and stomach load
	float scalediff = (prey_mdl.scale && pred_mdl.scale) ? (prey_mdl.scale / pred_mdl.scale) : 1; // offset swallow progress visual indication by scale difference
	float stomach_fill = (STAT(VORE_LOAD) && STAT(VORE_LOADMAX)) ? STAT(VORE_LOAD) / STAT(VORE_LOADMAX) : 0;
	float state = max(stomach_fill, min(1, STAT(VORE_PROGRESS_PRED) * scalediff));
	float dist = (-0.5 + state) * autocvar_cl_vore_model_external_range; // the model is centered at 0.5 progress
	vector viewangle = vec3(0, view_angles.y, 0);
	makevectors(viewangle);
	this.origin = view_origin + (v_forward * dist);
	this.angles = viewangle;

	// update other properties
	if(external.model != externalmodel)
	{
		_setmodel(this, externalmodel); // player model may change while the predator is registered
		this.frame = 0; // reset the animation if any
	}
	if(this.skin != pred.skin)
		this.skin = pred.skin; // player skin may change while the predator is registered
	if(this.colormap != pred.colormap)
		this.colormap = pred.colormap; // pants and shirt color
	if(this.glowmod != pred.glowmod)
		this.glowmod = pred.glowmod; // glow color
	this.alpha = autocvar_cl_vore_model_external;
}

// updates the vore models
void Vore_Models()
{
	// add the internal model when it's needed
	if(!internal && Vore_Models_Internal_Needed())
	{
		internal = spawn();
		internal.movetype = MOVETYPE_FOLLOW;
		internal.solid = SOLID_NOT;
		//internal.effects |= EF_NODEPTHTEST; // leave the depth test on, else it will clip the weapon and other players
		internal.drawmask = MASK_NORMAL;
		internal.draw = Vore_Models_Internal_Draw;
		IL_PUSH(g_drawables, internal);
	}

	// add the external model when it's needed
	if(!external && Vore_Models_External_Needed())
	{
		external = spawn();
		external.movetype = MOVETYPE_FOLLOW;
		external.solid = SOLID_NOT;
		external.effects |= EF_NODEPTHTEST;
		external.drawmask = MASK_NORMAL;
		external.draw = Vore_Models_External_Draw;
		IL_PUSH(g_drawables, external);
	}
}

// --------------------------------
// HUD functions:
// --------------------------------

void HUD_Stomach()
{
	// don't draw the panel if base conditions aren't met
	if(hud != HUD_NORMAL) return;
	if(!autocvar__hud_configure)
	{
		if(!autocvar_hud_panel_stomach || spectatee_status == -1)
			return;
		if(STAT(HEALTH) <= 0 && autocvar_hud_panel_stomach_hide_ondeath)
			return;
	}

	HUD_Panel_LoadCvars();

	if(autocvar_hud_panel_stomach_dynamichud)
		HUD_Scale_Enable();
	else
		HUD_Scale_Disable();
	HUD_Panel_DrawBg();

	// generate initial entries for all players
	// x = row, y = column, z = entnum
	vector boardslots[255]; // 255 is engine limit on maxclients
	int boardslots_total = -1; // arrays start from 0
	for(entity pl = players.sort_next; pl; pl = pl.sort_next)
	{
		if(!autocvar__hud_configure)
		{
			// ignore spectators
			if(pl.team == NUM_SPECTATOR)
				continue;

			// ignore players who are neither predators or prey
			entity this = entcs_receiver(pl.sv_entnum);
			if not(this.entcs_pred || this.entcs_load || this.entcs_progress_prey || this.entcs_progress_pred)
				continue;
		}

		++boardslots_total;
		boardslots[boardslots_total] = vec3(0, boardslots_total, pl.sv_entnum);

		// stop here if this is the maximum entry that can fit inside the panel
		float size_y = hud_fontsize.y * max(1, autocvar_hud_panel_stomach_icon);
		if(size_y * boardslots_total > panel_size.y)
			break;
	}

	// skip if there are no entries to display
	if(boardslots_total < 0)
		return;

	// player sorting, columns
	// determine how far a prey's predator chain goes, and store that as the prey's column
	// eg: if player a is swallowing player b who is swallowing player c, a = 0, b = 1, c = 2
	int boardslots_columns_max = bound(1, floor(autocvar_hud_panel_stomach_columns - 1), 15); // maximum number of columns, 16 is the hard limit
	int boardslots_columns_used = 0; // number of columns being used, set below
	for(int i = 0; i <= boardslots_total; ++i)
	{
		entity pl = entcs_receiver(boardslots[i].z); // the entity of i
		while(pl.entcs_pred > 0 && boardslots[i].x < boardslots_columns_max)
		{
			boardslots[i] += '1 0 0';
			pl = entcs_receiver(pl.entcs_pred - 1);

			// update the number of columns in use
			if(boardslots[i].x > boardslots_columns_used)
				boardslots_columns_used = boardslots[i].x;
		}
	}

	// player sorting, rows
	// loop through columns in order, ensuring that prey will always find their predator at the correct position
	// rows are then modified so that each prey is listed under their predator
	// note that the first column is skipped in the loop, as that's top level and has no predators to account for
	for(int column = 1; column <= boardslots_columns_used; ++column)
	{
		// loop through every potential prey
		for(int i = 0; i <= boardslots_total; ++i)
		{
			// skip if this prey isn't on our column of choice
			if(boardslots[i].x != column)
				continue;

			entity pl = entcs_receiver(boardslots[i].z); // the entity of i

			// loop through every potential predator
			for(int w = 0; w <= boardslots_total; ++w)
			{
				// skip if this predator isn't one unit below our column of choice
				if(i == w || boardslots[w].x != column - 1)
					continue;

				// confirm that this is our predator
				if(pl.entcs_pred - 1 == boardslots[w].z)
				{
					int row_old = boardslots[i].y;
					int row_new = boardslots[w].y + 1; // predator's row plus one
					if(row_old != row_new)
					{
						// loop through every entry in order to update it
						for(int q = 0; q <= boardslots_total; ++q)
						{
							// if this is our entry, set the new row
							if(q == i)
								boardslots[q] = vec3(boardslots[q].x, row_new, boardslots[q].z);

							// if this is another entry beyond or at the new row, increment the row by one unit to make room
							if(q != i && boardslots[q].y >= row_new)
								boardslots[q] += '0 1 0';

							// decrement the row of every entry beyond the old row by one unit to fill the gap
							if(boardslots[q].y > row_old)
								boardslots[q] -= '0 1 0';
						}
					}
				}
			}
		}
	}

	// determine the panel boundaries
	vector my_pos, my_size;
	my_pos = panel_pos;
	my_size = panel_size;
	if(panel_bg_padding)
	{
		my_pos += '1 1 0' * panel_bg_padding;
		my_size -= '2 2 0' * panel_bg_padding;
	}
	if not(autocvar_hud_panel_stomach_flip)
	{
		// align to the bottom instead of the top
		float size_y = hud_fontsize.y * max(1, autocvar_hud_panel_stomach_icon);
		my_pos.y += my_size.y - min((boardslots_total + 1) * size_y, my_size.y);
	}

	// draw the entry of each slot
	draw_beginBoldFont();
	for(int slot = 0; slot <= boardslots_total; ++slot)
	{
		int pl_entnum = boardslots[slot].z;
		entity pl_ent = entcs_receiver(pl_entnum);
		string pl_name = entcs_GetName(pl_entnum);
		int pl_team = entcs_GetTeam(pl_entnum);
		int pl_colormap = entcs_GetClientColors(pl_entnum);

		// setup the icon and rings
		vector icon_size = vec3(hud_fontsize.x * max(1, autocvar_hud_panel_stomach_icon), hud_fontsize.y * max(1, autocvar_hud_panel_stomach_icon), 0);
		vector icon_pos = vec3(my_pos.x + (boardslots[slot].x * icon_size.x), my_pos.y + (boardslots[slot].y * icon_size.y), 0);
		string icon_pic = string_null; // set below
		vector icon_ring_pos = vec3(icon_pos.x + (icon_size.x / 2), icon_pos.y + (icon_size.y / 2), 0);
		float icon_ring_size = max(icon_size.x, icon_size.y) * M_SQRT2; // sqrt(2)
		vector icon_ring_pred_color = '1 1 1'; // set below
		vector icon_ring_prey_color = '1 1 1'; // set below
		float icon_ring_pred_fill = 0; // set below
		float icon_ring_prey_fill = 0; // set below
		if(autocvar_hud_panel_stomach_icon >= 1)
		{
			// set the ring fill
			bool alt_pred = false; // set below
			bool alt_prey = false; // set below
			if(autocvar_hud_panel_stomach_ring_pred)
			{
				switch(autocvar_hud_panel_stomach_ring_pred)
				{
					case 1:
						// pred progress
						icon_ring_pred_fill = pl_ent.entcs_progress_pred;
						alt_pred = false;
						break;
					case 2:
						// stomach load
						icon_ring_pred_fill = pl_ent.entcs_load;
						alt_pred = true;
						break;
					case 3:
						// pred progress if any, stomach load if not
						icon_ring_pred_fill = pl_ent.entcs_progress_pred ? pl_ent.entcs_progress_pred : pl_ent.entcs_load;
						alt_pred = !pl_ent.entcs_progress_pred;
						break;
					case 4:
						// largest value between pred progress and stomach load
						icon_ring_pred_fill = max(pl_ent.entcs_progress_pred, pl_ent.entcs_load);
						alt_pred = pl_ent.entcs_progress_pred < pl_ent.entcs_load;
						break;
					case 5:
						// average value between pred progress and stomach load
						icon_ring_pred_fill = (pl_ent.entcs_progress_pred + pl_ent.entcs_load) / 2;
						alt_pred = !pl_ent.entcs_progress_pred;
						break;
				}
			}
			if(autocvar_hud_panel_stomach_ring_prey)
			{
				icon_ring_prey_fill = pl_ent.entcs_progress_prey;
				alt_prey = pl_ent.entcs_progress_prey >= 1;
			}

			// set the icon and ring colors
			if(pl_entnum == player_localentnum - 1)
			{
				icon_pic = strcat(hud_skin_path, "/stomach_sign_self");
				icon_ring_pred_color = alt_pred ? autocvar_hud_panel_stomach_ring_pred_color_self_load : autocvar_hud_panel_stomach_ring_pred_color_self_progress;
				icon_ring_prey_color = alt_prey ? autocvar_hud_panel_stomach_ring_prey_color_self_eaten : autocvar_hud_panel_stomach_ring_prey_color_self_progress;
			}
			else if(teamplay && pl_team == myteam)
			{
				icon_pic = strcat(hud_skin_path, "/stomach_sign_team");
				icon_ring_pred_color = alt_pred ? autocvar_hud_panel_stomach_ring_pred_color_team_load : autocvar_hud_panel_stomach_ring_pred_color_team_progress;
				icon_ring_prey_color = alt_prey ? autocvar_hud_panel_stomach_ring_prey_color_team_eaten : autocvar_hud_panel_stomach_ring_prey_color_team_progress;
			}
			else
			{
				icon_pic = strcat(hud_skin_path, "/stomach_sign_enemy");
				icon_ring_pred_color = alt_pred ? autocvar_hud_panel_stomach_ring_pred_color_enemy_load : autocvar_hud_panel_stomach_ring_pred_color_enemy_progress;
				icon_ring_prey_color = alt_prey ? autocvar_hud_panel_stomach_ring_prey_color_enemy_eaten : autocvar_hud_panel_stomach_ring_prey_color_enemy_progress;
			}
		}

		// setup the color
		string color_base_pic = "gfx/scoreboard/playercolor_base";
		string color_shirt_pic = "gfx/scoreboard/playercolor_shirt";
		string color_pants_pic = "gfx/scoreboard/playercolor_pants";
		vector color_shirt_rgb = colormapPaletteColor(floor(pl_colormap / 16), 0);
		vector color_pants_rgb = colormapPaletteColor(pl_colormap % 16, 1);
		vector color_size_image = draw_getimagesize(color_base_pic);
		float color_size_aspect = color_size_image.x / color_size_image.y;
		vector color_size = vec3(hud_fontsize.x * color_size_aspect, hud_fontsize.y, 0);
		vector color_pos = vec3((icon_pic ? icon_pos.x + icon_size.x : icon_pos.x), icon_pos.y + (icon_size.y / 2) - (color_size.y / 2), 0);

		// setup the text
		vector label_pos = vec3((color_base_pic && color_shirt_pic && color_pants_pic ? color_pos.x + color_size.x : color_pos.x), color_pos.y, 0);
		string label_text = textShortenToWidth(pl_name, max(0, (my_pos.x + my_size.x) - label_pos.x), hud_fontsize, stringwidth_colors);
		vector label_size = vec3(stringwidth(label_text, true, hud_fontsize), hud_fontsize.y, 0);

		// skip this entry if the column or the row poke out of the panel
		bool overflow_x = (label_pos.x + label_size.x > my_pos.x + my_size.x); // check the right edge of the rightmost element
		bool overflow_y = (icon_pos.y + icon_size.y > my_pos.y + my_size.y); // check the bottom edge of the tallest element
		if(overflow_x || overflow_y)
			continue;

		// draw the elements
		if(icon_pic)
		{
			if(boardslots[slot].x > 0 && icon_pos.x >= my_pos.x + icon_size.x)
				drawpic(icon_pos - vec3(icon_size.x, 0, 0), strcat(hud_skin_path, "/stomach_separator"), icon_size, '1 1 1', panel_fg_alpha, DRAWFLAG_NORMAL);
			drawpic(icon_pos, icon_pic, icon_size, '1 1 1', panel_fg_alpha, DRAWFLAG_NORMAL);
			if(icon_ring_pred_fill)
				DrawCircleClippedPic(icon_ring_pos, icon_ring_size, strcat(hud_skin_path, "/stomach_ring_pred"), icon_ring_pred_fill, icon_ring_pred_color, panel_fg_alpha, DRAWFLAG_NORMAL);
			if(icon_ring_prey_fill)
				DrawCircleClippedPic(icon_ring_pos, icon_ring_size, strcat(hud_skin_path, "/stomach_ring_prey"), icon_ring_prey_fill, icon_ring_prey_color, panel_fg_alpha, DRAWFLAG_NORMAL);
		}
		if(color_base_pic && color_shirt_pic && color_pants_pic)
		{
			drawpic(color_pos, color_base_pic, color_size, '1 1 1', panel_fg_alpha, DRAWFLAG_NORMAL);
			drawpic(color_pos, color_shirt_pic, color_size, color_shirt_rgb, panel_fg_alpha, DRAWFLAG_NORMAL);
			drawpic(color_pos, color_pants_pic, color_size, color_pants_rgb, panel_fg_alpha, DRAWFLAG_NORMAL);
		}
		drawcolorcodedstring(label_pos, label_text, hud_fontsize, panel_fg_alpha, DRAWFLAG_NORMAL);
	}
	draw_endBoldFont();
}

void HUD_Stomach_Export(int fh)
{
	HUD_Write_Cvar("hud_panel_stomach_icon");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred_color_self_progress");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred_color_team_progress");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred_color_enemy_progress");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred_color_self_load");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred_color_team_load");
	HUD_Write_Cvar("hud_panel_stomach_ring_pred_color_enemy_load");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey_color_self_progress");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey_color_team_progress");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey_color_enemy_progress");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey_color_self_eaten");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey_color_team_eaten");
	HUD_Write_Cvar("hud_panel_stomach_ring_prey_color_enemy_eaten");
	HUD_Write_Cvar("hud_panel_stomach_columns");
	HUD_Write_Cvar("hud_panel_stomach_flip");
}

// --------------------------------
// Hook functions:
// --------------------------------

void VoreMain_OnAdd()
{
	// precache the vore related player models
	if(autocvar_cl_vore_precacheplayermodels)
	{
		playermodel_precache("models/player/*_body.zym");
		playermodel_precache("models/player/*_body.dpm");
		playermodel_precache("models/player/*_body.md3");
		playermodel_precache("models/player/*_body.psk");
		playermodel_precache("models/player/*_body.iqm");

		playermodel_precache("models/player/*_mouth.zym");
		playermodel_precache("models/player/*_mouth.dpm");
		playermodel_precache("models/player/*_mouth.md3");
		playermodel_precache("models/player/*_mouth.psk");
		playermodel_precache("models/player/*_mouth.iqm");

		playermodel_precache("models/player/*_stomach.zym");
		playermodel_precache("models/player/*_stomach.dpm");
		playermodel_precache("models/player/*_stomach.md3");
		playermodel_precache("models/player/*_stomach.psk");
		playermodel_precache("models/player/*_stomach.iqm");
	}

	vore_volume_sound_real = cvar("volume");
	vore_volume_music_real = cvar("bgmvolume");
	vore_brightness_real = cvar("v_contrast");

	vore_volume_sound_new = 0;
	vore_volume_music_new = 0;
	vore_brightness_new = 0;
}

void VoreMain_OnRemove()
{
	vore_volume_sound_real = 0;
	vore_volume_music_real = 0;
	vore_brightness_real = 0;
}

// DrawCrosshair mutator hook, returns bool "draw"
bool VoreMain_DrawCrosshair_Draw()
{
	// vore rings indicating progress
	if(autocvar_cl_vore_ring && (STAT(VORE_PROGRESS_PRED) > 0 || STAT(VORE_PROGRESS_PREY) > 0))
	{
		// ring appearances and positions
		string ring_pred_name = "gfx/crosshair_ring_pred";
		string ring_prey_name = "gfx/crosshair_ring_prey";
		vector ring_pred_color = autocvar_cl_vore_ring_color; // set later
		vector ring_prey_color = autocvar_cl_vore_ring_color; // set later
		float ring_pred_alpha = autocvar_cl_vore_ring_alpha; // set later
		float ring_prey_alpha = autocvar_cl_vore_ring_alpha; // set later
		vector ring_pred_size = draw_getimagesize(ring_pred_name) * autocvar_cl_vore_ring_size;
		vector ring_prey_size = draw_getimagesize(ring_prey_name) * autocvar_cl_vore_ring_size;
		vector ring_pred_origin = ('0.5 0 0' * vid_conwidth + '0 0.5 0' * vid_conheight);
		vector ring_prey_origin = ('0.5 0 0' * vid_conwidth + '0 0.5 0' * vid_conheight);

		// copy vore crosshair indicator appearance
		if(autocvar_cl_vore_ring_copy)
		if(autocvar_cl_vore_crosshair && (STAT(VORE_CANPRED) > 0 || STAT(VORE_CANPREY) > 0))
		{
			// determine pred ring appearance
			if(STAT(VORE_CANPRED) == 3 || STAT(VORE_CANPRED) == 5)
			{
				ring_pred_color = autocvar_cl_vore_crosshair_indicator_eat_color;
				ring_pred_alpha = autocvar_cl_vore_crosshair_indicator_eat_alpha;
			}
			else if(STAT(VORE_CANPRED) == 2 || STAT(VORE_CANPRED) == 4)
			{
				ring_pred_color = autocvar_cl_vore_crosshair_indicator_swallow_color;
				ring_pred_alpha = autocvar_cl_vore_crosshair_indicator_swallow_alpha;
			}
			else if(STAT(VORE_CANPRED) == 1)
			{
				ring_pred_color = autocvar_cl_vore_crosshair_indicator_invalid_color;
				ring_pred_alpha = autocvar_cl_vore_crosshair_indicator_invalid_alpha;
			}

			// determine prey ring appearance
			if(STAT(VORE_CANPREY) == 3 || STAT(VORE_CANPREY) == 5)
			{
				ring_prey_color = autocvar_cl_vore_crosshair_indicator_eat_color;
				ring_prey_alpha = autocvar_cl_vore_crosshair_indicator_eat_alpha;
			}
			else if(STAT(VORE_CANPREY) == 2 || STAT(VORE_CANPREY) == 4)
			{
				ring_prey_color = autocvar_cl_vore_crosshair_indicator_swallow_color;
				ring_prey_alpha = autocvar_cl_vore_crosshair_indicator_swallow_alpha;
			}
			else if(STAT(VORE_CANPREY) == 1)
			{
				ring_prey_color = autocvar_cl_vore_crosshair_indicator_invalid_color;
				ring_prey_alpha = autocvar_cl_vore_crosshair_indicator_invalid_alpha;
			}
		}

		// draw the rings
		DrawCircleClippedPic(ring_pred_origin, ring_pred_size_x, ring_pred_name, STAT(VORE_PROGRESS_PRED), ring_pred_color, ring_pred_alpha, DRAWFLAG_NORMAL);
		DrawCircleClippedPic(ring_prey_origin, ring_prey_size_x, ring_prey_name, STAT(VORE_PROGRESS_PREY), ring_prey_color, ring_prey_alpha, DRAWFLAG_NORMAL);
	}

	// vore crosshair indicating possible actions
	if(autocvar_cl_vore_crosshair && (STAT(VORE_CANPRED) > 0 || STAT(VORE_CANPREY) > 0))
	{
		// update transition effect
		if(sign_effect_canpred != STAT(VORE_CANPRED))
		{
			sign_effect_time = time + autocvar_cl_vore_crosshair_time;
			sign_effect_canpred = STAT(VORE_CANPRED);
		}
		float sign_effect_position = 1 - bound(0, (time / sign_effect_time - 1) / ((sign_effect_time - autocvar_cl_vore_crosshair_time) / sign_effect_time - 1), 1);

		// sign and indicator appearances
		string sign_name = "gfx/crosshair_vore_sign_invalid"; // set later
		vector sign_color = autocvar_cl_vore_crosshair_color;
		float sign_alpha = autocvar_cl_vore_crosshair_alpha * sign_effect_position;
		string sign_indicator_pred_name = "gfx/crosshair_vore_indicator_pred";
		string sign_indicator_prey_name = "gfx/crosshair_vore_indicator_prey";
		vector sign_indicator_pred_color = autocvar_cl_vore_crosshair_color; // set later
		vector sign_indicator_prey_color = autocvar_cl_vore_crosshair_color; // set later
		float sign_indicator_pred_alpha = autocvar_cl_vore_crosshair_alpha * sign_effect_position; // set later
		float sign_indicator_prey_alpha = autocvar_cl_vore_crosshair_alpha * sign_effect_position; // set later

		// determine sign appearance
		if(STAT(VORE_CANPRED) == 4 || STAT(VORE_CANPRED) == 5 || STAT(VORE_CANPREY) == 4 || STAT(VORE_CANPREY) == 5)
			sign_name = "gfx/crosshair_vore_sign_team";
		else if(STAT(VORE_CANPRED) == 2 || STAT(VORE_CANPRED) == 3 || STAT(VORE_CANPREY) == 2 || STAT(VORE_CANPREY) == 3)
			sign_name = "gfx/crosshair_vore_sign_enemy";
		else if(STAT(VORE_CANPRED) == 1 || STAT(VORE_CANPREY) == 1)
			sign_name = "gfx/crosshair_vore_sign_invalid";

		// determine pred indicator appearance
		if(STAT(VORE_CANPRED) == 3 || STAT(VORE_CANPRED) == 5)
		{
			sign_indicator_pred_color = autocvar_cl_vore_crosshair_indicator_eat_color;
			sign_indicator_pred_alpha = autocvar_cl_vore_crosshair_indicator_eat_alpha * sign_effect_position;
		}
		else if(STAT(VORE_CANPRED) == 2 || STAT(VORE_CANPRED) == 4)
		{
			sign_indicator_pred_color = autocvar_cl_vore_crosshair_indicator_swallow_color;
			sign_indicator_pred_alpha = autocvar_cl_vore_crosshair_indicator_swallow_alpha * sign_effect_position;
		}
		else if(STAT(VORE_CANPRED) == 1)
		{
			sign_indicator_pred_color = autocvar_cl_vore_crosshair_indicator_invalid_color;
			sign_indicator_pred_alpha = autocvar_cl_vore_crosshair_indicator_invalid_alpha * sign_effect_position;
		}

		// determine prey indicator appearance
		if(STAT(VORE_CANPREY) == 3 || STAT(VORE_CANPREY) == 5)
		{
			sign_indicator_prey_color = autocvar_cl_vore_crosshair_indicator_eat_color;
			sign_indicator_prey_alpha = autocvar_cl_vore_crosshair_indicator_eat_alpha * sign_effect_position;
		}
		else if(STAT(VORE_CANPREY) == 2 || STAT(VORE_CANPREY) == 4)
		{
			sign_indicator_prey_color = autocvar_cl_vore_crosshair_indicator_swallow_color;
			sign_indicator_prey_alpha = autocvar_cl_vore_crosshair_indicator_swallow_alpha * sign_effect_position;
		}
		else if(STAT(VORE_CANPREY) == 1)
		{
			sign_indicator_prey_color = autocvar_cl_vore_crosshair_indicator_invalid_color;
			sign_indicator_prey_alpha = autocvar_cl_vore_crosshair_indicator_invalid_alpha * sign_effect_position;
		}

		// sign and indicator positions
		vector sign_size = draw_getimagesize(sign_name) * autocvar_cl_vore_crosshair_size * sign_effect_position;
		vector sign_origin = ('0.5 0 0' * vid_conwidth + '0 0.5 0' * vid_conheight) - (sign_size * 0.5);
		vector sign_indicator_pred_size = draw_getimagesize(sign_indicator_pred_name) * autocvar_cl_vore_crosshair_size * sign_effect_position;
		vector sign_indicator_prey_size = draw_getimagesize(sign_indicator_prey_name) * autocvar_cl_vore_crosshair_size * sign_effect_position;
		vector sign_indicator_pred_origin = ('0.5 0 0' * vid_conwidth + '0 0.5 0' * vid_conheight) - (sign_indicator_pred_size * 0.5);
		vector sign_indicator_prey_origin = ('0.5 0 0' * vid_conwidth + '0 0.5 0' * vid_conheight) - (sign_indicator_prey_size * 0.5);

		// draw the sign and indicators
		drawpic(sign_origin, sign_name, sign_size, sign_color, sign_alpha, DRAWFLAG_NORMAL);
		if(STAT(VORE_CANPRED) > 0)
			drawpic(sign_indicator_pred_origin, sign_indicator_pred_name, sign_indicator_pred_size, sign_indicator_pred_color, sign_indicator_pred_alpha, DRAWFLAG_NORMAL);
		if(STAT(VORE_CANPREY) > 0)
			drawpic(sign_indicator_prey_origin, sign_indicator_prey_name, sign_indicator_prey_size, sign_indicator_prey_color, sign_indicator_prey_alpha, DRAWFLAG_NORMAL);

		return true;
	}
	else
		sign_effect_time = sign_effect_canpred = 0;

	return false;
}

// DrawScoreboard mutator hook, returns bool "draw"
bool VoreMain_DrawScoreboard_Draw()
{
	// report client variables to the server if they changed
	if(autocvar_chase_active != report_cvar_chase_active)
	{
		localcmd("\nsendcvar chase_active\n");
		report_cvar_chase_active = autocvar_chase_active;
	}
	if(autocvar_cl_vore_autodigest != report_cvar_cl_vore_autodigest)
	{
		localcmd("\nsendcvar cl_vore_autodigest\n");
		report_cvar_cl_vore_autodigest = autocvar_cl_vore_autodigest;
	}

	// hide the world from prey players, if their view is completely obstructed
	// cvars modified here: r_drawworld
	// TODO: Achieve this without modifying cvars later on, setting VF_DRAWWORLD is one way but it's overwritten from here
	if(IS_PREY() && !button_zoom && !autocvar_chase_active && autocvar_cl_vore_model_internal >= 1)
	{
		if(vore_drawworld)
		{
			vore_drawworld = false;
			cvar_settemp("r_drawworld", "0");
		}
	}
	else
	{
		if(!vore_drawworld)
		{
			vore_drawworld = true;
			cvar_settemp("r_drawworld", "1");
		}
	}

	// copy predator appearance to prey
	if(IS_PREY() && STAT(HEALTH) <= 0)
	{
		if(!vore_copy)
		{
			entity pred = ENT_PRED();
			if(autocvar_cl_vore_copy_color >= random() && !teamplay)
				cvar_settemp("_cl_color", ftos(pred.colormap));
			if(autocvar_cl_vore_copy_model >= random())
				cvar_settemp("_cl_playermodel", strcat(playermodel_pure(pred.model), ".iqm"));
			if(autocvar_cl_vore_copy_skin >= random())
				cvar_settemp("_cl_playerskin", ftos(pred.skin));
			vore_copy = true;
		}
	}
	else
		vore_copy = false;

	// trigger the vore flash
	if(autocvar_cl_vore_flash)
	{
		int vore_flash_state = -1;
		if(!IS_PREY())
			vore_flash_state = STAT(VORE_PLAYERS);

		// only update the flash if our target is the same player
		if not(spectatee_status && vore_flash_lastspec != spectatee_status)
		{
			if(vore_flash_state > vore_flash_laststate && vore_flash_state > 0) // if stomach load is bigger we ate someone
			{
				vore_flash_color = autocvar_cl_vore_flash_pred_color;
				vore_flash_alpha = autocvar_cl_vore_flash_pred_alpha;
				vore_flash_time = time + autocvar_cl_vore_flash;
			}
			else if(vore_flash_state < vore_flash_laststate && vore_flash_state < 0) // -1 means we have been eaten
			{
				vore_flash_color = autocvar_cl_vore_flash_prey_color;
				vore_flash_alpha = autocvar_cl_vore_flash_prey_alpha;
				vore_flash_time = time + autocvar_cl_vore_flash;
			}
		}

		// draw the flash
		if(vore_flash_time > time)
		{
			float vore_flash_position = bound(0, (time / vore_flash_time - 1) / ((vore_flash_time - autocvar_cl_vore_flash) / vore_flash_time - 1), 1);
			drawfill('0 0 0', ('1 0 0' * vid_conwidth + '0 1 0' * vid_conheight), vore_flash_color, vore_flash_alpha * vore_flash_position, DRAWFLAG_ADDITIVE);
		}

		// update the last state and spectatee status, otherwise the flash may be triggered when switching spectated players due to detecting a different stomach load
		vore_flash_laststate = vore_flash_state;
		vore_flash_lastspec = spectatee_status;
	}

	// draw the fluid overlay
	if(autocvar_cl_vore_fluid)
	{
		float amount = IS_PREY() ? autocvar_cl_vore_fluid_add_prey : STAT(VORE_PROGRESS_PREY) * autocvar_cl_vore_fluid_add_progress;
		if(STAT(HEALTH) <= 0 || spectatee_status == -1)
			fluid_density = 0;
		else if(fluid_density < amount)
			fluid_density = amount;

		if(fluid_density > 0)
		{
			vector fluid_size = vec3(max(vid_conwidth, vid_conheight), max(vid_conwidth, vid_conheight), 0);
			vector fluid_pos = vec3((vid_conwidth - fluid_size.x) / 2, (vid_conheight - fluid_size.y) / 2, 0);
			vector fluid_color = autocvar_cl_vore_fluid_color;
			float fluid_alpha = bound(0, fluid_density * autocvar_cl_vore_fluid_alpha, 1);
			drawpic(fluid_pos, "gfx/fluid", fluid_size, fluid_color, fluid_alpha, DRAWFLAG_NORMAL);

			if(time > fluid_time)
			{
				fluid_density = max(0, fluid_density - autocvar_cl_vore_fluid * frametime);
				fluid_time = time + EFFECT_DELAY;
			}
		}
	}

	// update sound volume and brightness
	// since this modifies menu settings, only make changes while the menu is closed, and revert them once the menu is opened
	// cvars modified here: volume, v_contrast
	// TODO: Achieve this without modifying cvars later on, especially ones used in the menu
	if(autocvar__menu_alpha)
	{
		// if the menu was just opened, read the real cvars, otherwise update them
		if(vore_volume_sound_new || vore_volume_music_new || vore_brightness_new)
		{
			// menu newly opened, revert menu settings to their normal values
			cvar_settemp("volume", ftos(vore_volume_sound_real));
			cvar_settemp("bgmvolume", ftos(vore_volume_music_real));
			cvar_settemp("v_contrast", ftos(vore_brightness_real));
			localcmd("menu_sync\n"); // make sure the real cvars are seen by the menu
			
			vore_volume_sound_new = vore_volume_music_new = vore_brightness_new = 0;
		}
		else
		{
			// menu already opened, real cvars already correct and not darkened/muffled
			// make sure real values are kept updated
			vore_volume_sound_real = cvar("volume");
			vore_volume_music_real = cvar("bgmvolume");
			vore_brightness_real = cvar("v_contrast");
		}
	}
	else
	{
		// calculate the modified volume and brightness
		vore_volume_sound_new = vore_volume_sound_real;
		vore_volume_music_new = vore_volume_music_real;
		vore_brightness_new = vore_brightness_real;
		if(spectatee_status != -1)
		{
			if(autocvar_cl_vore_decrease_volume_sound_swallow && STAT(VORE_PROGRESS_PREY))
				vore_volume_sound_new *= 1 - (STAT(VORE_PROGRESS_PREY) * autocvar_cl_vore_decrease_volume_sound_swallow);
			if(autocvar_cl_vore_decrease_volume_sound_prey && IS_PREY())
				vore_volume_sound_new *= 1 - autocvar_cl_vore_decrease_volume_sound_prey;
			if(autocvar_cl_vore_decrease_volume_music_swallow && STAT(VORE_PROGRESS_PREY))
				vore_volume_music_new *= 1 - (STAT(VORE_PROGRESS_PREY) * autocvar_cl_vore_decrease_volume_music_swallow);
			if(autocvar_cl_vore_decrease_volume_music_prey && IS_PREY())
				vore_volume_music_new *= 1 - autocvar_cl_vore_decrease_volume_music_prey;
			if(autocvar_cl_vore_decrease_brightness_swallow && STAT(VORE_PROGRESS_PREY))
				vore_brightness_new *= 1 - (STAT(VORE_PROGRESS_PREY) * autocvar_cl_vore_decrease_brightness_swallow);
			if(autocvar_cl_vore_decrease_brightness_prey && IS_PREY())
				vore_brightness_new *= 1 - autocvar_cl_vore_decrease_brightness_prey;
		}

		// apply the modified volume and brightness if they changed
		if(cvar("volume") != vore_volume_sound_new)
			cvar_settemp("volume", ftos(vore_volume_sound_new));
		if(cvar("bgmvolume") != vore_volume_music_new)
			cvar_settemp("bgmvolume", ftos(vore_volume_music_new));
		if(cvar("v_contrast") != vore_brightness_new)
			cvar_settemp("v_contrast", ftos(vore_brightness_new));
	}

	// update models
	Vore_Models();

	return false;
}

// WantEventchase mutator hook, returns bool "want"
bool VoreMain_WantEventchase_Want()
{
	if(autocvar_cl_vore_eventchase_prey && IS_PREY())
		return true;

	return false;
}

// HUD_Contents mutator hook, returns bool "forbid"
bool VoreMain_HUD_Contents_Forbid()
{
	// don't trigger the HUD blur effect if our predator is underwater, in case the predator is still alive
	if(IS_PREY() && !entcs_IsDead(fabs(STAT(VORE_PRED)) - 1))
		return true;

	return false;
}
